/*  Copyright (C) 2018, Axis Communications AB, LUND, SWEDEN
 *
 *  This file is part of RAPP.
 *
 *  RAPP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  You can use the comments under either the terms of the GNU Lesser General
 *  Public License version 3 as published by the Free Software Foundation,
 *  either version 3 of the License or (at your option) any later version, or
 *  the GNU Free Documentation License version 1.3 or any later version
 *  published by the Free Software Foundation; with no Invariant Sections, no
 *  Front-Cover Texts, and no Back-Cover Texts.
 *  A copy of the license is included in the documentation section entitled
 *  "GNU Free Documentation License".
 *
 *  RAPP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License and a copy of the GNU Free Documentation License along
 *  with RAPP. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *  @file   rc_vec_neon128.h
 *  @brief  RAPP Compute layer vector operations
 *          using the Aarch64 128-bit NEON instruction set.
 */

#ifndef RC_VEC_NEON128_H
#define RC_VEC_NEON128_H

#ifndef RC_VECTOR_H
#error "Do not include this file directly! Use rc_vector.h instead."
#endif /* !RC_VECTOR_H */

#include <arm_neon.h>

/* Local support macros */

/* Vector to scalar */
#define RC_TVEC_(t, x) \
    (((union {t v; int64_t i[2]; })(t)(x)).i[0])

#define RC_ZERO_ vdupq_n_u8(0)

/* See the porting documentation for generic comments. */

/**
 *  128-bit NEON for Aarch64 has native instructions for all implemented
 *  hintable backend macros, so no hint-macros apply.
 */

/**
 *  Here, 128-bit NEON instructions are used.
 *  See rc_vec_neon_aarc64.h for the 64-bit back-end.
 */

typedef uint8x16_t rc_vec_t;

#define RC_VEC_SIZE 16

#define RC_VEC_DECLARE()

#define RC_VEC_CLEANUP()

#define RC_VEC_LOAD(vec, ptr) \
    ((vec) = vld1q_u8((const uint8_t *)(ptr)))

#define RC_VEC_STORE(ptr, vec) \
    vst1q_u8((uint8_t *)(ptr), vec)

#define RC_VEC_LDINIT(vec1, vec2, vec3, uptr, ptr) \
do {                                               \
    (void)(vec1);                                  \
    (void)(vec2);                                  \
    (void)(vec3);                                  \
    (uptr) = (ptr);                                \
} while (0)

#define RC_VEC_LOADU(dstv, vec1, vec2, vec3, uptr) \
    ((dstv) = vld1q_u8(uptr))

/**
 *  No RC_VEC_SHR nor RC_VEC_SHL and thus no need to define
 *  RC_VEC_SHINIT. There's no whole-vector shift by-bits (i.e. no
 *  "vshlq_u128") and the argument to "vextq_u8" must be a constant.
 *  We could store and load at an offset, but that'd be just excessive.
 *  Luckily at the time of this writing, RC_VEC_SHR is unused and
 *  RC_VEC_SHL is only used together with RC_VEC_GETMASKV and there's an
 *  option to define RC_VEC_GETMASKW instead and make the use go
 *  away.
 */

#define RC_VEC_SHLC(dstv, srcv, bytes)  \
    ((dstv) = vextq_u8(srcv, RC_ZERO_, bytes))

/**
 *  The conditional is required as an error-message otherwise tells us
 *  that 16 is out-of-range.
 */
#define RC_VEC_SHRC(dstv, srcv, bytes)  \
    ((dstv) = (bytes) == 0 ? (srcv) : vextq_u8(RC_ZERO_, srcv, 16 - (bytes)))

#define RC_VEC_ALIGNC(dstv, srcv1, srcv2, bytes)    \
    ((dstv) = vextq_u8(srcv1, srcv2, bytes))

#define RC_VEC_PACK(dstv, srcv1, srcv2)         \
    ((dstv) = vuzp1q_u8(srcv1, srcv2))

#define RC_VEC_ZERO(vec) \
    ((vec) = vdupq_n_u8(0))

#define RC_VEC_NOT(dstv, srcv) \
    ((dstv) = vmvnq_u8(srcv))

#define RC_VEC_AND(dstv, srcv1, srcv2) \
    ((dstv) = vandq_u8(srcv1, srcv2))

#define RC_VEC_OR(dstv, srcv1, srcv2) \
    ((dstv) = vorrq_u8(srcv1, srcv2))

#define RC_VEC_XOR(dstv, srcv1, srcv2) \
    ((dstv) = veorq_u8(srcv1, srcv2))

#define RC_VEC_ANDNOT(dstv, srcv1, srcv2) \
    ((dstv) = vbicq_u8(srcv1, srcv2))

#define RC_VEC_ORNOT(dstv, srcv1, srcv2) \
    ((dstv) = vornq_u8(srcv1, srcv2))

#define RC_VEC_XORNOT(dstv, srcv1, srcv2) \
do {                                      \
    rc_vec_t not_;                        \
    RC_VEC_NOT(not_, srcv2);              \
    RC_VEC_XOR(dstv, srcv1, not_);        \
} while (0)

#define RC_VEC_SPLAT(vec, scal) \
    ((vec) = vdupq_n_u8(scal))

#define RC_VEC_ADDS(dstv, srcv1, srcv2) \
    ((dstv) = vqaddq_u8(srcv1, srcv2))

#define RC_VEC_AVGT(dstv, srcv1, srcv2) \
    ((dstv) = vhaddq_u8(srcv1, srcv2))

#define RC_VEC_AVGR(dstv, srcv1, srcv2) \
    ((dstv) = vrhaddq_u8(srcv1, srcv2))

#define RC_VEC_AVGZ(dstv, srcv1, srcv2)     \
do {                                        \
    rc_vec_t sv1_ = (srcv1);                \
    rc_vec_t sv2_ = (srcv2);                \
    rc_vec_t adj_1_, adj_xor_, adj_, avg_;  \
    rc_vec_t cmp_, adj_cmpx_;               \
    RC_VEC_SPLAT(adj_1_, 1);                \
    RC_VEC_CMPGT(cmp_, sv2_, sv1_);         \
    RC_VEC_XOR(adj_xor_, sv1_, sv2_);       \
    RC_VEC_AVGR(avg_, sv1_, sv2_);          \
    RC_VEC_AND(adj_cmpx_, adj_xor_, cmp_);  \
    RC_VEC_AND(adj_, adj_cmpx_, adj_1_);    \
    (dstv) = vsubq_u8(avg_, adj_);           \
} while (0)

#define RC_VEC_SUBS(dstv, srcv1, srcv2) \
    ((dstv) = vqsubq_u8(srcv1, srcv2))

#define RC_VEC_SUBA(dstv, srcv1, srcv2) \
    ((dstv) = vabdq_u8(srcv1, srcv2))

#define RC_VEC_SUBHT(dstv, srcv1, srcv2)    \
do {                                        \
    rc_vec_t notv2_;                        \
    RC_VEC_NOT(notv2_, srcv2);              \
    RC_VEC_AVGT(dstv, srcv1, notv2_);       \
} while (0)

#define RC_VEC_SUBHR(dstv, srcv1, srcv2)    \
do {                                        \
    rc_vec_t notv2_;                        \
    RC_VEC_NOT(notv2_, srcv2);              \
    RC_VEC_AVGR(dstv, srcv1, notv2_);       \
} while (0)

#define RC_VEC_ABS(dstv, srcv)              \
do {                                        \
    rc_vec_t bias_, abs1_;                  \
    RC_VEC_SPLAT(bias_, 0x80);              \
    RC_VEC_SUBA(abs1_, srcv, bias_);        \
    RC_VEC_ADDS(dstv, abs1_, abs1_);        \
} while (0)

#define RC_VEC_CMPGT(dstv, srcv1, srcv2) \
    ((dstv) = vcgtq_u8(srcv1, srcv2))

#define RC_VEC_CMPGE(dstv, srcv1, srcv2) \
    ((dstv) = vcgeq_u8(srcv1, srcv2))

#define RC_VEC_MIN(dstv, srcv1, srcv2) \
    ((dstv) = vminq_u8(srcv1, srcv2))

#define RC_VEC_MAX(dstv, srcv1, srcv2) \
    ((dstv) = vmaxq_u8(srcv1, srcv2))

#define RC_VEC_LERP_(dstv, srcv1, srcv2, blendv, biaslo, biashi)    \
do {                                                                \
    int16x8_t bv_ = (int16x8_t)(blendv);                            \
    rc_vec_t sv1_ = (srcv1);                                        \
    rc_vec_t sv2_ = (srcv2);                                        \
    rc_vec_t rs_;                                                   \
    int16x8_t lo1_, hi1_, lo2_, hi2_;                               \
    /* Expand to 16 bits. */                                        \
    lo1_ = (int16x8_t)vzip1q_u8(sv1_, RC_ZERO_);                    \
    hi1_ = (int16x8_t)vzip2q_u8(sv1_, RC_ZERO_);                    \
    lo2_ = (int16x8_t)vzip1q_u8(sv2_, RC_ZERO_);                    \
    hi2_ = (int16x8_t)vzip2q_u8(sv2_, RC_ZERO_);                    \
    /* Do (srcv1 - srcv2) as 16 bits. */                            \
    int16x8_t lod_ = vsubq_s16(lo2_, lo1_);                         \
    int16x8_t hid_ = vsubq_s16(hi2_, hi1_);                         \
    /* ... * blendv ... */                                          \
    int16x8_t lobld_ = vmulq_s16(lod_, bv_);                        \
    int16x8_t hibld_ = vmulq_s16(hid_, bv_);                        \
    /* ... + bias = (srcv1 - srcv2) * blendv + bias */              \
    int16x8_t los_ = vaddq_s16(lobld_, biaslo);                     \
    int16x8_t his_ = vaddq_s16(hibld_, biashi);                     \
    /* Reduce to 8 bits. */                                         \
    los_ = vshrq_n_s16(los_, 8);                                    \
    his_ = vshrq_n_s16(his_, 8);                                    \
    RC_VEC_PACK(rs_, (rc_vec_t)los_, (rc_vec_t)his_);               \
    /* Finally, add srcv1. */                                       \
    (dstv) = vaddq_u8(rs_, sv1_);                                   \
} while (0)

#define RC_VEC_BLEND(blendv, blend8) \
    ((blendv) = (rc_vec_t)vdupq_n_s16(blend8))

#define RC_VEC_LERP(dstv, srcv1, srcv2, blend8, blendv)     \
do {                                                        \
    int16x8_t bias_ = vdupq_n_s16(0x80);                    \
    RC_VEC_LERP_(dstv, srcv1, srcv2, blendv, bias_, bias_); \
} while (0)

#define RC_VEC_BLENDZ(blendv, blend8) \
    RC_VEC_BLEND(blendv, blend8)

#define RC_VEC_LERPZ(dstv, srcv1, srcv2, blend8, blendv)    \
do {                                                        \
    int16x8_t blo_, bhi_;                                   \
    rc_vec_t srcv1_ = (srcv1);                              \
    rc_vec_t srcv2_ = (srcv2);                              \
    rc_vec_t bias_;                                         \
    RC_VEC_CMPGT(bias_, srcv1_, srcv2_);                    \
    blo_ = (int16x8_t)vzip1q_u8(bias_, RC_ZERO_);           \
    bhi_ = (int16x8_t)vzip2q_u8(bias_, RC_ZERO_);           \
    RC_VEC_LERP_(dstv, srcv1_, srcv2_, blendv, blo_, bhi_); \
} while (0)

#define RC_VEC_BLENDN(blendv, blend8)   \
    RC_VEC_BLEND(blendv, blend8)

#define RC_VEC_LERPN(dstv, srcv1, srcv2, blend8, blendv)    \
do {                                                        \
    int16x8_t blo_, bhi_;                                   \
    rc_vec_t srcv1_ = (srcv1);                              \
    rc_vec_t srcv2_ = (srcv2);                              \
    rc_vec_t bias_;                                         \
    RC_VEC_CMPGT(bias_, srcv2_, srcv1_);                    \
    blo_ = (int16x8_t)vzip1q_u8(bias_, RC_ZERO_);           \
    bhi_ = (int16x8_t)vzip2q_u8(bias_, RC_ZERO_);           \
    RC_VEC_LERP_(dstv, srcv1_, srcv2_, blendv, blo_, bhi_); \
} while (0)

/**
 *  Beware: until we have RC_VEC_SHL (or vectorized functions call for it),
 *  RC_VEC_GETMASKV won't actually be used.
 */
#define RC_VEC_GETMASKV(maskv, vec)                     \
do {                                                    \
    int8x16_t rcshv_ = {-7, -6, -5, -4, -3, -2, -1, 0,  \
                        -7, -6, -5, -4, -3, -2, -1, 0}; \
    rc_vec_t b7_ = vandq_u8(vec, vdupq_n_u8(0x80));     \
    rc_vec_t b7_0_ = vshlq_u8(b7_, rcshv_);             \
    rc_vec_t lobyte_ = (rc_vec_t)                       \
        vpaddlq_u32(vpaddlq_u16(vpaddlq_u8(b7_0_)));    \
    rc_vec_t loall_ = vdupq_laneq_u8(lobyte_, 0);       \
    (maskv) = vzip2q_u8(loall_, lobyte_);               \
} while (0)

#define RC_VEC_GETMASKW(maskw, vec)                     \
do {                                                    \
    int8x16_t rcshv_ = {-7, -6, -5, -4, -3, -2, -1, 0,  \
                        -7, -6, -5, -4, -3, -2, -1, 0}; \
    rc_vec_t b7_ = vandq_u8(vec, vdupq_n_u8(0x80));     \
    rc_vec_t b7_0_ = vshlq_u8(b7_, rcshv_);             \
    rc_vec_t lobyte_ = (rc_vec_t)                       \
        vpaddlq_u32(vpaddlq_u16(vpaddlq_u8(b7_0_)));    \
    (maskw) = vgetq_lane_u8(lobyte_, 8) * 256 +         \
        vgetq_lane_u8(lobyte_, 0);                      \
} while (0)

#define RC_VEC_SETMASKV(vec, maskv)                             \
do {                                                            \
    rc_vec_t maskv_ = (maskv);                                  \
    rc_vec_t mask_ = (rc_vec_t){1<<0, 1<<1, 1<<2, 1<<3,         \
                                1<<4, 1<<5, 1<<6, 1<<7,         \
                                1<<0, 1<<1, 1<<2, 1<<3,         \
                                1<<4, 1<<5, 1<<6, 1<<7};        \
    rc_vec_t indx0_ = vdupq_laneq_u8(maskv_, 0);                \
    rc_vec_t indx1_ = vdupq_laneq_u8(maskv_, 1);                \
    rc_vec_t indx01_ = vextq_u8(indx0_, indx1_, 8);             \
    (vec) = vtstq_u8(indx01_, mask_);                           \
} while (0)

#define RC_VEC_SUMN 128 /* floor(UINT16_MAX/510) = 128 */

#define RC_VEC_SUMV(accv, srcv)             \
do {                                        \
    uint16x8_t accv_ = (uint16x8_t)(accv);  \
    /* Parallel add and accumulate */       \
    accv_ = vpadalq_u8(accv_, srcv);        \
    (accv) = (rc_vec_t)accv_;               \
} while (0)

#define RC_VEC_SUMR(sum, accv)                                          \
do {                                                                    \
    uint16x8_t accv_ = (uint16x8_t)(accv);                              \
    uint64x2_t sumr_;                                                   \
    sumr_ = vpaddlq_u32(vpaddlq_u16(accv_));                            \
    (sum) = vgetq_lane_u64(sumr_, 0) + vgetq_lane_u64(sumr_, 1);        \
} while (0)

/* floor(UINT16_MAX/16) - (floor(UINT16_MAX/16) % 4) = 4092 */
#define RC_VEC_CNTN 4092

#define RC_VEC_CNTV(accv, srcv) \
    RC_VEC_SUMV(accv, vcntq_u8(srcv))

#define RC_VEC_CNTR(cnt, accv) \
    RC_VEC_SUMR(cnt, accv)

/* Our intermediate container is uint32_t, same as the result: no overflow. */
#define RC_VEC_MACN 1024

#define RC_VEC_MACV(accv, srcv1, srcv2)                         \
do {                                                            \
    uint32x4_t accv1_, accv2_, accv0_ = (uint32x4_t)(accv);     \
    uint16x8_t prod0_, prod1_;                                  \
    prod0_ = vmull_u8(vget_low_u8(srcv1), vget_low_u8(srcv2));  \
    prod1_ = vmull_high_u8(srcv1, srcv2);                       \
    accv1_ = accv0_ + vpaddlq_u16(prod0_);                      \
    accv2_ = accv1_ + vpaddlq_u16(prod1_);                      \
    (accv) = (rc_vec_t)accv2_;                                  \
} while (0)

#define RC_VEC_MACR(mac, accv)                                          \
do {                                                                    \
    uint64x2_t accv_ = vpaddlq_u32((uint32x4_t)(accv));                 \
    (mac) = vgetq_lane_u64(accv_, 0) + vgetq_lane_u64(accv_, 1);        \
} while (0)

#endif /* RC_VEC_NEON_A64_16_H */
