/**
 *  @file   rapptune.h
 *  @brief  RAPP Compute implementation tuning config.
 *          Hacked tune-file to enable all implemented vectorized functions.
 */

#ifndef RAPPTUNE_H
#define RAPPTUNE_H

#include "rc_impl.h" /* Implementation names */

#define rc_compiler_version 0

#define rc_bitblt_wm_copy_bin_IMPL                           RC_IMPL_GEN
#define rc_bitblt_wm_copy_bin_UNROLL                         1
#define rc_bitblt_wm_copy_bin_SCORE                          0.0

#define rc_bitblt_wm_not_bin_IMPL                            RC_IMPL_GEN
#define rc_bitblt_wm_not_bin_UNROLL                          1
#define rc_bitblt_wm_not_bin_SCORE                           0.0

#define rc_bitblt_wm_and_bin_IMPL                            RC_IMPL_GEN
#define rc_bitblt_wm_and_bin_UNROLL                          1
#define rc_bitblt_wm_and_bin_SCORE                           0.0

#define rc_bitblt_wm_or_bin_IMPL                             RC_IMPL_GEN
#define rc_bitblt_wm_or_bin_UNROLL                           1
#define rc_bitblt_wm_or_bin_SCORE                            0.0

#define rc_bitblt_wm_xor_bin_IMPL                            RC_IMPL_GEN
#define rc_bitblt_wm_xor_bin_UNROLL                          1
#define rc_bitblt_wm_xor_bin_SCORE                           0.0

#define rc_bitblt_wm_nand_bin_IMPL                           RC_IMPL_GEN
#define rc_bitblt_wm_nand_bin_UNROLL                         1
#define rc_bitblt_wm_nand_bin_SCORE                          0.0

#define rc_bitblt_wm_nor_bin_IMPL                            RC_IMPL_GEN
#define rc_bitblt_wm_nor_bin_UNROLL                          1
#define rc_bitblt_wm_nor_bin_SCORE                           0.0

#define rc_bitblt_wm_xnor_bin_IMPL                           RC_IMPL_GEN
#define rc_bitblt_wm_xnor_bin_UNROLL                         1
#define rc_bitblt_wm_xnor_bin_SCORE                          0.0

#define rc_bitblt_wm_andn_bin_IMPL                           RC_IMPL_GEN
#define rc_bitblt_wm_andn_bin_UNROLL                         1
#define rc_bitblt_wm_andn_bin_SCORE                          0.0

#define rc_bitblt_wm_orn_bin_IMPL                            RC_IMPL_GEN
#define rc_bitblt_wm_orn_bin_UNROLL                          1
#define rc_bitblt_wm_orn_bin_SCORE                           0.0

#define rc_bitblt_wm_nandn_bin_IMPL                          RC_IMPL_GEN
#define rc_bitblt_wm_nandn_bin_UNROLL                        1
#define rc_bitblt_wm_nandn_bin_SCORE                         0.0

#define rc_bitblt_wm_norn_bin_IMPL                           RC_IMPL_GEN
#define rc_bitblt_wm_norn_bin_UNROLL                         1
#define rc_bitblt_wm_norn_bin_SCORE                          0.0

#define rc_bitblt_wa_copy_bin_IMPL                           RC_IMPL_GEN
#define rc_bitblt_wa_copy_bin_UNROLL                         1
#define rc_bitblt_wa_copy_bin_SCORE                          0.0

#define rc_bitblt_wa_not_bin_IMPL                            RC_IMPL_GEN
#define rc_bitblt_wa_not_bin_UNROLL                          1
#define rc_bitblt_wa_not_bin_SCORE                           0.0

#define rc_bitblt_wa_and_bin_IMPL                            RC_IMPL_GEN
#define rc_bitblt_wa_and_bin_UNROLL                          1
#define rc_bitblt_wa_and_bin_SCORE                           0.0

#define rc_bitblt_wa_or_bin_IMPL                             RC_IMPL_GEN
#define rc_bitblt_wa_or_bin_UNROLL                           1
#define rc_bitblt_wa_or_bin_SCORE                            0.0

#define rc_bitblt_wa_xor_bin_IMPL                            RC_IMPL_GEN
#define rc_bitblt_wa_xor_bin_UNROLL                          1
#define rc_bitblt_wa_xor_bin_SCORE                           0.0

#define rc_bitblt_wa_nand_bin_IMPL                           RC_IMPL_GEN
#define rc_bitblt_wa_nand_bin_UNROLL                         1
#define rc_bitblt_wa_nand_bin_SCORE                          0.0

#define rc_bitblt_wa_nor_bin_IMPL                            RC_IMPL_GEN
#define rc_bitblt_wa_nor_bin_UNROLL                          1
#define rc_bitblt_wa_nor_bin_SCORE                           0.0

#define rc_bitblt_wa_xnor_bin_IMPL                           RC_IMPL_GEN
#define rc_bitblt_wa_xnor_bin_UNROLL                         1
#define rc_bitblt_wa_xnor_bin_SCORE                          0.0

#define rc_bitblt_wa_andn_bin_IMPL                           RC_IMPL_GEN
#define rc_bitblt_wa_andn_bin_UNROLL                         1
#define rc_bitblt_wa_andn_bin_SCORE                          0.0

#define rc_bitblt_wa_orn_bin_IMPL                            RC_IMPL_GEN
#define rc_bitblt_wa_orn_bin_UNROLL                          1
#define rc_bitblt_wa_orn_bin_SCORE                           0.0

#define rc_bitblt_wa_nandn_bin_IMPL                          RC_IMPL_GEN
#define rc_bitblt_wa_nandn_bin_UNROLL                        1
#define rc_bitblt_wa_nandn_bin_SCORE                         0.0

#define rc_bitblt_wa_norn_bin_IMPL                           RC_IMPL_GEN
#define rc_bitblt_wa_norn_bin_UNROLL                         1
#define rc_bitblt_wa_norn_bin_SCORE                          0.0

#define rc_bitblt_vm_copy_bin_IMPL                           RC_IMPL_SIMD
#define rc_bitblt_vm_copy_bin_UNROLL                         1
#define rc_bitblt_vm_copy_bin_SCORE                          0.0

#define rc_bitblt_vm_not_bin_IMPL                            RC_IMPL_SIMD
#define rc_bitblt_vm_not_bin_UNROLL                          1
#define rc_bitblt_vm_not_bin_SCORE                           0.0

#define rc_bitblt_vm_and_bin_IMPL                            RC_IMPL_SIMD
#define rc_bitblt_vm_and_bin_UNROLL                          1
#define rc_bitblt_vm_and_bin_SCORE                           0.0

#define rc_bitblt_vm_or_bin_IMPL                             RC_IMPL_SIMD
#define rc_bitblt_vm_or_bin_UNROLL                           1
#define rc_bitblt_vm_or_bin_SCORE                            0.0

#define rc_bitblt_vm_xor_bin_IMPL                            RC_IMPL_SIMD
#define rc_bitblt_vm_xor_bin_UNROLL                          1
#define rc_bitblt_vm_xor_bin_SCORE                           0.0

#define rc_bitblt_vm_nand_bin_IMPL                           RC_IMPL_SIMD
#define rc_bitblt_vm_nand_bin_UNROLL                         1
#define rc_bitblt_vm_nand_bin_SCORE                          0.0

#define rc_bitblt_vm_nor_bin_IMPL                            RC_IMPL_SIMD
#define rc_bitblt_vm_nor_bin_UNROLL                          1
#define rc_bitblt_vm_nor_bin_SCORE                           0.0

#define rc_bitblt_vm_xnor_bin_IMPL                           RC_IMPL_SIMD
#define rc_bitblt_vm_xnor_bin_UNROLL                         1
#define rc_bitblt_vm_xnor_bin_SCORE                          0.0

#define rc_bitblt_vm_andn_bin_IMPL                           RC_IMPL_SIMD
#define rc_bitblt_vm_andn_bin_UNROLL                         1
#define rc_bitblt_vm_andn_bin_SCORE                          0.0

#define rc_bitblt_vm_orn_bin_IMPL                            RC_IMPL_SIMD
#define rc_bitblt_vm_orn_bin_UNROLL                          1
#define rc_bitblt_vm_orn_bin_SCORE                           0.0

#define rc_bitblt_vm_nandn_bin_IMPL                          RC_IMPL_SIMD
#define rc_bitblt_vm_nandn_bin_UNROLL                        1
#define rc_bitblt_vm_nandn_bin_SCORE                         0.0

#define rc_bitblt_vm_norn_bin_IMPL                           RC_IMPL_SIMD
#define rc_bitblt_vm_norn_bin_UNROLL                         1
#define rc_bitblt_vm_norn_bin_SCORE                          0.0

#define rc_bitblt_va_copy_bin_IMPL                           RC_IMPL_SIMD
#define rc_bitblt_va_copy_bin_UNROLL                         1
#define rc_bitblt_va_copy_bin_SCORE                          0.0

#define rc_bitblt_va_not_bin_IMPL                            RC_IMPL_SIMD
#define rc_bitblt_va_not_bin_UNROLL                          1
#define rc_bitblt_va_not_bin_SCORE                           0.0

#define rc_bitblt_va_and_bin_IMPL                            RC_IMPL_SIMD
#define rc_bitblt_va_and_bin_UNROLL                          1
#define rc_bitblt_va_and_bin_SCORE                           0.0

#define rc_bitblt_va_or_bin_IMPL                             RC_IMPL_SIMD
#define rc_bitblt_va_or_bin_UNROLL                           1
#define rc_bitblt_va_or_bin_SCORE                            0.0

#define rc_bitblt_va_xor_bin_IMPL                            RC_IMPL_SIMD
#define rc_bitblt_va_xor_bin_UNROLL                          1
#define rc_bitblt_va_xor_bin_SCORE                           0.0

#define rc_bitblt_va_nand_bin_IMPL                           RC_IMPL_SIMD
#define rc_bitblt_va_nand_bin_UNROLL                         1
#define rc_bitblt_va_nand_bin_SCORE                          0.0

#define rc_bitblt_va_nor_bin_IMPL                            RC_IMPL_SIMD
#define rc_bitblt_va_nor_bin_UNROLL                          1
#define rc_bitblt_va_nor_bin_SCORE                           0.0

#define rc_bitblt_va_xnor_bin_IMPL                           RC_IMPL_SIMD
#define rc_bitblt_va_xnor_bin_UNROLL                         1
#define rc_bitblt_va_xnor_bin_SCORE                          0.0

#define rc_bitblt_va_andn_bin_IMPL                           RC_IMPL_SIMD
#define rc_bitblt_va_andn_bin_UNROLL                         1
#define rc_bitblt_va_andn_bin_SCORE                          0.0

#define rc_bitblt_va_orn_bin_IMPL                            RC_IMPL_SIMD
#define rc_bitblt_va_orn_bin_UNROLL                          1
#define rc_bitblt_va_orn_bin_SCORE                           0.0

#define rc_bitblt_va_nandn_bin_IMPL                          RC_IMPL_SIMD
#define rc_bitblt_va_nandn_bin_UNROLL                        1
#define rc_bitblt_va_nandn_bin_SCORE                         0.0

#define rc_bitblt_va_norn_bin_IMPL                           RC_IMPL_SIMD
#define rc_bitblt_va_norn_bin_UNROLL                         1
#define rc_bitblt_va_norn_bin_SCORE                          0.0

#define rc_pixop_set_u8_IMPL                                 RC_IMPL_SIMD
#define rc_pixop_set_u8_UNROLL                               1
#define rc_pixop_set_u8_SCORE                                0.0

#define rc_pixop_not_u8_IMPL                                 RC_IMPL_SIMD
#define rc_pixop_not_u8_UNROLL                               1
#define rc_pixop_not_u8_SCORE                                0.0

#define rc_pixop_flip_u8_IMPL                                RC_IMPL_SIMD
#define rc_pixop_flip_u8_UNROLL                              1
#define rc_pixop_flip_u8_SCORE                               0.0

#define rc_pixop_lut_u8_IMPL                                 RC_IMPL_GEN
#define rc_pixop_lut_u8_UNROLL                               1
#define rc_pixop_lut_u8_SCORE                                0.0

#define rc_pixop_abs_u8_IMPL                                 RC_IMPL_SIMD
#define rc_pixop_abs_u8_UNROLL                               1
#define rc_pixop_abs_u8_SCORE                                0.0

#define rc_pixop_addc_u8_IMPL                                RC_IMPL_SIMD
#define rc_pixop_addc_u8_UNROLL                              1
#define rc_pixop_addc_u8_SCORE                               0.0

#define rc_pixop_lerpc_u8_IMPL                               RC_IMPL_SIMD
#define rc_pixop_lerpc_u8_UNROLL                             1
#define rc_pixop_lerpc_u8_SCORE                              0.0

#define rc_pixop_lerpnc_u8_IMPL                              RC_IMPL_SIMD
#define rc_pixop_lerpnc_u8_UNROLL                            1
#define rc_pixop_lerpnc_u8_SCORE                             0.0

#define rc_pixop_add_u8_IMPL                                 RC_IMPL_SIMD
#define rc_pixop_add_u8_UNROLL                               1
#define rc_pixop_add_u8_SCORE                                0.0

#define rc_pixop_avg_u8_IMPL                                 RC_IMPL_SIMD
#define rc_pixop_avg_u8_UNROLL                               1
#define rc_pixop_avg_u8_SCORE                                0.0

#define rc_pixop_sub_u8_IMPL                                 RC_IMPL_SIMD
#define rc_pixop_sub_u8_UNROLL                               1
#define rc_pixop_sub_u8_SCORE                                0.0

#define rc_pixop_subh_u8_IMPL                                RC_IMPL_SIMD
#define rc_pixop_subh_u8_UNROLL                              1
#define rc_pixop_subh_u8_SCORE                               0.0

#define rc_pixop_suba_u8_IMPL                                RC_IMPL_SIMD
#define rc_pixop_suba_u8_UNROLL                              1
#define rc_pixop_suba_u8_SCORE                               0.0

#define rc_pixop_lerp_u8_IMPL                                RC_IMPL_SIMD
#define rc_pixop_lerp_u8_UNROLL                              1
#define rc_pixop_lerp_u8_SCORE                               0.0

#define rc_pixop_lerpn_u8_IMPL                               RC_IMPL_SIMD
#define rc_pixop_lerpn_u8_UNROLL                             1
#define rc_pixop_lerpn_u8_SCORE                              0.0

#define rc_pixop_lerpi_u8_IMPL                               RC_IMPL_SIMD
#define rc_pixop_lerpi_u8_UNROLL                             1
#define rc_pixop_lerpi_u8_SCORE                              0.0

#define rc_pixop_norm_u8_IMPL                                RC_IMPL_SIMD
#define rc_pixop_norm_u8_UNROLL                              1
#define rc_pixop_norm_u8_SCORE                               0.0

#define rc_type_u8_to_bin_IMPL                               RC_IMPL_SIMD
#define rc_type_u8_to_bin_UNROLL                             1
#define rc_type_u8_to_bin_SCORE                              0.0

#define rc_type_bin_to_u8_IMPL                               RC_IMPL_SIMD
#define rc_type_bin_to_u8_UNROLL                             1
#define rc_type_bin_to_u8_SCORE                              0.0

#define rc_thresh_gt_u8_IMPL                                 RC_IMPL_SIMD
#define rc_thresh_gt_u8_UNROLL                               1
#define rc_thresh_gt_u8_SCORE                                0.0

#define rc_thresh_lt_u8_IMPL                                 RC_IMPL_SIMD
#define rc_thresh_lt_u8_UNROLL                               1
#define rc_thresh_lt_u8_SCORE                                0.0

#define rc_thresh_gtlt_u8_IMPL                               RC_IMPL_SIMD
#define rc_thresh_gtlt_u8_UNROLL                             1
#define rc_thresh_gtlt_u8_SCORE                              0.0

#define rc_thresh_ltgt_u8_IMPL                               RC_IMPL_SIMD
#define rc_thresh_ltgt_u8_UNROLL                             1
#define rc_thresh_ltgt_u8_SCORE                              0.0

#define rc_thresh_gt_pixel_u8_IMPL                           RC_IMPL_SIMD
#define rc_thresh_gt_pixel_u8_UNROLL                         1
#define rc_thresh_gt_pixel_u8_SCORE                          0.0

#define rc_thresh_lt_pixel_u8_IMPL                           RC_IMPL_SIMD
#define rc_thresh_lt_pixel_u8_UNROLL                         1
#define rc_thresh_lt_pixel_u8_SCORE                          0.0

#define rc_thresh_gtlt_pixel_u8_IMPL                         RC_IMPL_SIMD
#define rc_thresh_gtlt_pixel_u8_UNROLL                       1
#define rc_thresh_gtlt_pixel_u8_SCORE                        0.0

#define rc_thresh_ltgt_pixel_u8_IMPL                         RC_IMPL_SIMD
#define rc_thresh_ltgt_pixel_u8_UNROLL                       1
#define rc_thresh_ltgt_pixel_u8_SCORE                        0.0

#define rc_stat_sum_bin_IMPL                                 RC_IMPL_SIMD
#define rc_stat_sum_bin_UNROLL                               1
#define rc_stat_sum_bin_SCORE                                0.0

#define rc_stat_sum_u8_IMPL                                  RC_IMPL_SIMD
#define rc_stat_sum_u8_UNROLL                                1
#define rc_stat_sum_u8_SCORE                                 0.0

#define rc_stat_sum2_u8_IMPL                                 RC_IMPL_SIMD
#define rc_stat_sum2_u8_UNROLL                               1
#define rc_stat_sum2_u8_SCORE                                0.0

#define rc_stat_xsum_u8_IMPL                                 RC_IMPL_SIMD
#define rc_stat_xsum_u8_UNROLL                               1
#define rc_stat_xsum_u8_SCORE                                0.0

#define rc_stat_min_bin_IMPL                                 RC_IMPL_SIMD
#define rc_stat_min_bin_UNROLL                               1
#define rc_stat_min_bin_SCORE                                0.0

#define rc_stat_max_bin_IMPL                                 RC_IMPL_SIMD
#define rc_stat_max_bin_UNROLL                               1
#define rc_stat_max_bin_SCORE                                0.0

#define rc_stat_min_u8_IMPL                                  RC_IMPL_SIMD
#define rc_stat_min_u8_UNROLL                                1
#define rc_stat_min_u8_SCORE                                 0.0

#define rc_stat_max_u8_IMPL                                  RC_IMPL_SIMD
#define rc_stat_max_u8_UNROLL                                1
#define rc_stat_max_u8_SCORE                                 0.0

#define rc_reduce_1x2_u8_IMPL                                RC_IMPL_SIMD
#define rc_reduce_1x2_u8_UNROLL                              1
#define rc_reduce_1x2_u8_SCORE                               0.0

#define rc_reduce_2x1_u8_IMPL                                RC_IMPL_SIMD
#define rc_reduce_2x1_u8_UNROLL                              1
#define rc_reduce_2x1_u8_SCORE                               0.0

#define rc_reduce_2x2_u8_IMPL                                RC_IMPL_SIMD
#define rc_reduce_2x2_u8_UNROLL                              1
#define rc_reduce_2x2_u8_SCORE                               0.0

#define rc_reduce_1x2_rk1_bin_IMPL                           RC_IMPL_GEN
#define rc_reduce_1x2_rk1_bin_UNROLL                         1
#define rc_reduce_1x2_rk1_bin_SCORE                          0.0

#define rc_reduce_1x2_rk2_bin_IMPL                           RC_IMPL_GEN
#define rc_reduce_1x2_rk2_bin_UNROLL                         1
#define rc_reduce_1x2_rk2_bin_SCORE                          0.0

#define rc_reduce_2x1_rk1_bin_IMPL                           RC_IMPL_GEN
#define rc_reduce_2x1_rk1_bin_UNROLL                         1
#define rc_reduce_2x1_rk1_bin_SCORE                          0.0

#define rc_reduce_2x1_rk2_bin_IMPL                           RC_IMPL_GEN
#define rc_reduce_2x1_rk2_bin_UNROLL                         1
#define rc_reduce_2x1_rk2_bin_SCORE                          0.0

#define rc_reduce_2x2_rk1_bin_IMPL                           RC_IMPL_GEN
#define rc_reduce_2x2_rk1_bin_UNROLL                         1
#define rc_reduce_2x2_rk1_bin_SCORE                          0.0

#define rc_reduce_2x2_rk2_bin_IMPL                           RC_IMPL_GEN
#define rc_reduce_2x2_rk2_bin_UNROLL                         1
#define rc_reduce_2x2_rk2_bin_SCORE                          0.0

#define rc_reduce_2x2_rk3_bin_IMPL                           RC_IMPL_GEN
#define rc_reduce_2x2_rk3_bin_UNROLL                         1
#define rc_reduce_2x2_rk3_bin_SCORE                          0.0

#define rc_reduce_2x2_rk4_bin_IMPL                           RC_IMPL_GEN
#define rc_reduce_2x2_rk4_bin_UNROLL                         1
#define rc_reduce_2x2_rk4_bin_SCORE                          0.0

#define rc_expand_1x2_bin_IMPL                               RC_IMPL_GEN
#define rc_expand_1x2_bin_UNROLL                             1
#define rc_expand_1x2_bin_SCORE                              0.0

#define rc_expand_2x1_bin_IMPL                               RC_IMPL_GEN
#define rc_expand_2x1_bin_UNROLL                             1
#define rc_expand_2x1_bin_SCORE                              0.0

#define rc_expand_2x2_bin_IMPL                               RC_IMPL_GEN
#define rc_expand_2x2_bin_UNROLL                             1
#define rc_expand_2x2_bin_SCORE                              0.0

#define rc_rotate_cw_u8_IMPL                                 RC_IMPL_GEN
#define rc_rotate_cw_u8_UNROLL                               1
#define rc_rotate_cw_u8_SCORE                                0.0

#define rc_rotate_ccw_u8_IMPL                                RC_IMPL_GEN
#define rc_rotate_ccw_u8_UNROLL                              1
#define rc_rotate_ccw_u8_SCORE                               0.0

#define rc_filter_diff_1x2_horz_u8_IMPL                      RC_IMPL_SIMD
#define rc_filter_diff_1x2_horz_u8_UNROLL                    1
#define rc_filter_diff_1x2_horz_u8_SCORE                     0.0

#define rc_filter_diff_1x2_horz_abs_u8_IMPL                  RC_IMPL_SIMD
#define rc_filter_diff_1x2_horz_abs_u8_UNROLL                1
#define rc_filter_diff_1x2_horz_abs_u8_SCORE                 0.0

#define rc_filter_diff_2x1_vert_u8_IMPL                      RC_IMPL_SIMD
#define rc_filter_diff_2x1_vert_u8_UNROLL                    1
#define rc_filter_diff_2x1_vert_u8_SCORE                     0.0

#define rc_filter_diff_2x1_vert_abs_u8_IMPL                  RC_IMPL_SIMD
#define rc_filter_diff_2x1_vert_abs_u8_UNROLL                1
#define rc_filter_diff_2x1_vert_abs_u8_SCORE                 0.0

#define rc_filter_diff_2x2_magn_u8_IMPL                      RC_IMPL_SIMD
#define rc_filter_diff_2x2_magn_u8_UNROLL                    1
#define rc_filter_diff_2x2_magn_u8_SCORE                     0.0

#define rc_filter_sobel_3x3_horz_u8_IMPL                     RC_IMPL_SIMD
#define rc_filter_sobel_3x3_horz_u8_UNROLL                   1
#define rc_filter_sobel_3x3_horz_u8_SCORE                    0.0

#define rc_filter_sobel_3x3_horz_abs_u8_IMPL                 RC_IMPL_SIMD
#define rc_filter_sobel_3x3_horz_abs_u8_UNROLL               1
#define rc_filter_sobel_3x3_horz_abs_u8_SCORE                0.0

#define rc_filter_sobel_3x3_vert_u8_IMPL                     RC_IMPL_SIMD
#define rc_filter_sobel_3x3_vert_u8_UNROLL                   1
#define rc_filter_sobel_3x3_vert_u8_SCORE                    0.0

#define rc_filter_sobel_3x3_vert_abs_u8_IMPL                 RC_IMPL_SIMD
#define rc_filter_sobel_3x3_vert_abs_u8_UNROLL               1
#define rc_filter_sobel_3x3_vert_abs_u8_SCORE                0.0

#define rc_filter_sobel_3x3_magn_u8_IMPL                     RC_IMPL_SIMD
#define rc_filter_sobel_3x3_magn_u8_UNROLL                   1
#define rc_filter_sobel_3x3_magn_u8_SCORE                    0.0

#define rc_filter_gauss_3x3_u8_IMPL                          RC_IMPL_SIMD
#define rc_filter_gauss_3x3_u8_UNROLL                        1
#define rc_filter_gauss_3x3_u8_SCORE                         0.0

#define rc_filter_laplace_3x3_u8_IMPL                        RC_IMPL_SIMD
#define rc_filter_laplace_3x3_u8_UNROLL                      1
#define rc_filter_laplace_3x3_u8_SCORE                       0.0

#define rc_filter_laplace_3x3_abs_u8_IMPL                    RC_IMPL_SIMD
#define rc_filter_laplace_3x3_abs_u8_UNROLL                  1
#define rc_filter_laplace_3x3_abs_u8_SCORE                   0.0

#define rc_filter_highpass_3x3_u8_IMPL                       RC_IMPL_SIMD
#define rc_filter_highpass_3x3_u8_UNROLL                     1
#define rc_filter_highpass_3x3_u8_SCORE                      0.0

#define rc_filter_highpass_3x3_abs_u8_IMPL                   RC_IMPL_SIMD
#define rc_filter_highpass_3x3_abs_u8_UNROLL                 1
#define rc_filter_highpass_3x3_abs_u8_SCORE                  0.0

#define rc_morph_erode_line_1x2_bin_IMPL                     RC_IMPL_GEN
#define rc_morph_erode_line_1x2_bin_UNROLL                   1
#define rc_morph_erode_line_1x2_bin_SCORE                    0.0

#define rc_morph_dilate_line_1x2_bin_IMPL                    RC_IMPL_GEN
#define rc_morph_dilate_line_1x2_bin_UNROLL                  1
#define rc_morph_dilate_line_1x2_bin_SCORE                   0.0

#define rc_morph_erode_line_1x3_bin_IMPL                     RC_IMPL_GEN
#define rc_morph_erode_line_1x3_bin_UNROLL                   1
#define rc_morph_erode_line_1x3_bin_SCORE                    0.0

#define rc_morph_dilate_line_1x3_bin_IMPL                    RC_IMPL_GEN
#define rc_morph_dilate_line_1x3_bin_UNROLL                  1
#define rc_morph_dilate_line_1x3_bin_SCORE                   0.0

#define rc_morph_erode_line_1x3_p_bin_IMPL                   RC_IMPL_GEN
#define rc_morph_erode_line_1x3_p_bin_UNROLL                 1
#define rc_morph_erode_line_1x3_p_bin_SCORE                  0.0

#define rc_morph_dilate_line_1x3_p_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_dilate_line_1x3_p_bin_UNROLL                1
#define rc_morph_dilate_line_1x3_p_bin_SCORE                 0.0

#define rc_morph_erode_line_1x5_p_bin_IMPL                   RC_IMPL_GEN
#define rc_morph_erode_line_1x5_p_bin_UNROLL                 1
#define rc_morph_erode_line_1x5_p_bin_SCORE                  0.0

#define rc_morph_dilate_line_1x5_p_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_dilate_line_1x5_p_bin_UNROLL                1
#define rc_morph_dilate_line_1x5_p_bin_SCORE                 0.0

#define rc_morph_erode_line_1x7_p_bin_IMPL                   RC_IMPL_GEN
#define rc_morph_erode_line_1x7_p_bin_UNROLL                 1
#define rc_morph_erode_line_1x7_p_bin_SCORE                  0.0

#define rc_morph_dilate_line_1x7_p_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_dilate_line_1x7_p_bin_UNROLL                1
#define rc_morph_dilate_line_1x7_p_bin_SCORE                 0.0

#define rc_morph_erode_line_1x9_p_bin_IMPL                   RC_IMPL_GEN
#define rc_morph_erode_line_1x9_p_bin_UNROLL                 1
#define rc_morph_erode_line_1x9_p_bin_SCORE                  0.0

#define rc_morph_dilate_line_1x9_p_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_dilate_line_1x9_p_bin_UNROLL                1
#define rc_morph_dilate_line_1x9_p_bin_SCORE                 0.0

#define rc_morph_erode_line_1x13_p_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_erode_line_1x13_p_bin_UNROLL                1
#define rc_morph_erode_line_1x13_p_bin_SCORE                 0.0

#define rc_morph_dilate_line_1x13_p_bin_IMPL                 RC_IMPL_GEN
#define rc_morph_dilate_line_1x13_p_bin_UNROLL               1
#define rc_morph_dilate_line_1x13_p_bin_SCORE                0.0

#define rc_morph_erode_line_1x15_p_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_erode_line_1x15_p_bin_UNROLL                1
#define rc_morph_erode_line_1x15_p_bin_SCORE                 0.0

#define rc_morph_dilate_line_1x15_p_bin_IMPL                 RC_IMPL_GEN
#define rc_morph_dilate_line_1x15_p_bin_UNROLL               1
#define rc_morph_dilate_line_1x15_p_bin_SCORE                0.0

#define rc_morph_erode_line_1x17_p_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_erode_line_1x17_p_bin_UNROLL                1
#define rc_morph_erode_line_1x17_p_bin_SCORE                 0.0

#define rc_morph_dilate_line_1x17_p_bin_IMPL                 RC_IMPL_GEN
#define rc_morph_dilate_line_1x17_p_bin_UNROLL               1
#define rc_morph_dilate_line_1x17_p_bin_SCORE                0.0

#define rc_morph_erode_line_1x25_p_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_erode_line_1x25_p_bin_UNROLL                1
#define rc_morph_erode_line_1x25_p_bin_SCORE                 0.0

#define rc_morph_dilate_line_1x25_p_bin_IMPL                 RC_IMPL_GEN
#define rc_morph_dilate_line_1x25_p_bin_UNROLL               1
#define rc_morph_dilate_line_1x25_p_bin_SCORE                0.0

#define rc_morph_erode_line_1x29_p_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_erode_line_1x29_p_bin_UNROLL                1
#define rc_morph_erode_line_1x29_p_bin_SCORE                 0.0

#define rc_morph_dilate_line_1x29_p_bin_IMPL                 RC_IMPL_GEN
#define rc_morph_dilate_line_1x29_p_bin_UNROLL               1
#define rc_morph_dilate_line_1x29_p_bin_SCORE                0.0

#define rc_morph_erode_line_1x31_p_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_erode_line_1x31_p_bin_UNROLL                1
#define rc_morph_erode_line_1x31_p_bin_SCORE                 0.0

#define rc_morph_dilate_line_1x31_p_bin_IMPL                 RC_IMPL_GEN
#define rc_morph_dilate_line_1x31_p_bin_UNROLL               1
#define rc_morph_dilate_line_1x31_p_bin_SCORE                0.0

#define rc_morph_erode_line_2x1_bin_IMPL                     RC_IMPL_GEN
#define rc_morph_erode_line_2x1_bin_UNROLL                   1
#define rc_morph_erode_line_2x1_bin_SCORE                    0.0

#define rc_morph_dilate_line_2x1_bin_IMPL                    RC_IMPL_GEN
#define rc_morph_dilate_line_2x1_bin_UNROLL                  1
#define rc_morph_dilate_line_2x1_bin_SCORE                   0.0

#define rc_morph_erode_line_3x1_bin_IMPL                     RC_IMPL_GEN
#define rc_morph_erode_line_3x1_bin_UNROLL                   1
#define rc_morph_erode_line_3x1_bin_SCORE                    0.0

#define rc_morph_dilate_line_3x1_bin_IMPL                    RC_IMPL_GEN
#define rc_morph_dilate_line_3x1_bin_UNROLL                  1
#define rc_morph_dilate_line_3x1_bin_SCORE                   0.0

#define rc_morph_erode_line_3x1_p_bin_IMPL                   RC_IMPL_GEN
#define rc_morph_erode_line_3x1_p_bin_UNROLL                 1
#define rc_morph_erode_line_3x1_p_bin_SCORE                  0.0

#define rc_morph_dilate_line_3x1_p_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_dilate_line_3x1_p_bin_UNROLL                1
#define rc_morph_dilate_line_3x1_p_bin_SCORE                 0.0

#define rc_morph_erode_line_5x1_p_bin_IMPL                   RC_IMPL_GEN
#define rc_morph_erode_line_5x1_p_bin_UNROLL                 1
#define rc_morph_erode_line_5x1_p_bin_SCORE                  0.0

#define rc_morph_dilate_line_5x1_p_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_dilate_line_5x1_p_bin_UNROLL                1
#define rc_morph_dilate_line_5x1_p_bin_SCORE                 0.0

#define rc_morph_erode_line_7x1_p_bin_IMPL                   RC_IMPL_GEN
#define rc_morph_erode_line_7x1_p_bin_UNROLL                 1
#define rc_morph_erode_line_7x1_p_bin_SCORE                  0.0

#define rc_morph_dilate_line_7x1_p_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_dilate_line_7x1_p_bin_UNROLL                1
#define rc_morph_dilate_line_7x1_p_bin_SCORE                 0.0

#define rc_morph_erode_line_9x1_p_bin_IMPL                   RC_IMPL_GEN
#define rc_morph_erode_line_9x1_p_bin_UNROLL                 1
#define rc_morph_erode_line_9x1_p_bin_SCORE                  0.0

#define rc_morph_dilate_line_9x1_p_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_dilate_line_9x1_p_bin_UNROLL                1
#define rc_morph_dilate_line_9x1_p_bin_SCORE                 0.0

#define rc_morph_erode_line_13x1_p_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_erode_line_13x1_p_bin_UNROLL                1
#define rc_morph_erode_line_13x1_p_bin_SCORE                 0.0

#define rc_morph_dilate_line_13x1_p_bin_IMPL                 RC_IMPL_GEN
#define rc_morph_dilate_line_13x1_p_bin_UNROLL               1
#define rc_morph_dilate_line_13x1_p_bin_SCORE                0.0

#define rc_morph_erode_line_15x1_p_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_erode_line_15x1_p_bin_UNROLL                1
#define rc_morph_erode_line_15x1_p_bin_SCORE                 0.0

#define rc_morph_dilate_line_15x1_p_bin_IMPL                 RC_IMPL_GEN
#define rc_morph_dilate_line_15x1_p_bin_UNROLL               1
#define rc_morph_dilate_line_15x1_p_bin_SCORE                0.0

#define rc_morph_erode_line_17x1_p_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_erode_line_17x1_p_bin_UNROLL                1
#define rc_morph_erode_line_17x1_p_bin_SCORE                 0.0

#define rc_morph_dilate_line_17x1_p_bin_IMPL                 RC_IMPL_GEN
#define rc_morph_dilate_line_17x1_p_bin_UNROLL               1
#define rc_morph_dilate_line_17x1_p_bin_SCORE                0.0

#define rc_morph_erode_line_25x1_p_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_erode_line_25x1_p_bin_UNROLL                1
#define rc_morph_erode_line_25x1_p_bin_SCORE                 0.0

#define rc_morph_dilate_line_25x1_p_bin_IMPL                 RC_IMPL_GEN
#define rc_morph_dilate_line_25x1_p_bin_UNROLL               1
#define rc_morph_dilate_line_25x1_p_bin_SCORE                0.0

#define rc_morph_erode_line_29x1_p_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_erode_line_29x1_p_bin_UNROLL                1
#define rc_morph_erode_line_29x1_p_bin_SCORE                 0.0

#define rc_morph_dilate_line_29x1_p_bin_IMPL                 RC_IMPL_GEN
#define rc_morph_dilate_line_29x1_p_bin_UNROLL               1
#define rc_morph_dilate_line_29x1_p_bin_SCORE                0.0

#define rc_morph_erode_line_31x1_p_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_erode_line_31x1_p_bin_UNROLL                1
#define rc_morph_erode_line_31x1_p_bin_SCORE                 0.0

#define rc_morph_dilate_line_31x1_p_bin_IMPL                 RC_IMPL_GEN
#define rc_morph_dilate_line_31x1_p_bin_UNROLL               1
#define rc_morph_dilate_line_31x1_p_bin_SCORE                0.0

#define rc_morph_erode_square_2x2_bin_IMPL                   RC_IMPL_GEN
#define rc_morph_erode_square_2x2_bin_UNROLL                 1
#define rc_morph_erode_square_2x2_bin_SCORE                  0.0

#define rc_morph_dilate_square_2x2_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_dilate_square_2x2_bin_UNROLL                1
#define rc_morph_dilate_square_2x2_bin_SCORE                 0.0

#define rc_morph_erode_square_3x3_bin_IMPL                   RC_IMPL_GEN
#define rc_morph_erode_square_3x3_bin_UNROLL                 1
#define rc_morph_erode_square_3x3_bin_SCORE                  0.0

#define rc_morph_dilate_square_3x3_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_dilate_square_3x3_bin_UNROLL                1
#define rc_morph_dilate_square_3x3_bin_SCORE                 0.0

#define rc_morph_erode_square_3x3_p_bin_IMPL                 RC_IMPL_GEN
#define rc_morph_erode_square_3x3_p_bin_UNROLL               1
#define rc_morph_erode_square_3x3_p_bin_SCORE                0.0

#define rc_morph_dilate_square_3x3_p_bin_IMPL                RC_IMPL_GEN
#define rc_morph_dilate_square_3x3_p_bin_UNROLL              1
#define rc_morph_dilate_square_3x3_p_bin_SCORE               0.0

#define rc_morph_erode_diamond_3x3_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_erode_diamond_3x3_bin_UNROLL                1
#define rc_morph_erode_diamond_3x3_bin_SCORE                 0.0

#define rc_morph_dilate_diamond_3x3_bin_IMPL                 RC_IMPL_GEN
#define rc_morph_dilate_diamond_3x3_bin_UNROLL               1
#define rc_morph_dilate_diamond_3x3_bin_SCORE                0.0

#define rc_morph_erode_diamond_3x3_p_bin_IMPL                RC_IMPL_GEN
#define rc_morph_erode_diamond_3x3_p_bin_UNROLL              1
#define rc_morph_erode_diamond_3x3_p_bin_SCORE               0.0

#define rc_morph_dilate_diamond_3x3_p_bin_IMPL               RC_IMPL_GEN
#define rc_morph_dilate_diamond_3x3_p_bin_UNROLL             1
#define rc_morph_dilate_diamond_3x3_p_bin_SCORE              0.0

#define rc_morph_erode_diamond_5x5_p_bin_IMPL                RC_IMPL_GEN
#define rc_morph_erode_diamond_5x5_p_bin_UNROLL              1
#define rc_morph_erode_diamond_5x5_p_bin_SCORE               0.0

#define rc_morph_dilate_diamond_5x5_p_bin_IMPL               RC_IMPL_GEN
#define rc_morph_dilate_diamond_5x5_p_bin_UNROLL             1
#define rc_morph_dilate_diamond_5x5_p_bin_SCORE              0.0

#define rc_morph_erode_diamond_7x7_p_bin_IMPL                RC_IMPL_GEN
#define rc_morph_erode_diamond_7x7_p_bin_UNROLL              1
#define rc_morph_erode_diamond_7x7_p_bin_SCORE               0.0

#define rc_morph_dilate_diamond_7x7_p_bin_IMPL               RC_IMPL_GEN
#define rc_morph_dilate_diamond_7x7_p_bin_UNROLL             1
#define rc_morph_dilate_diamond_7x7_p_bin_SCORE              0.0

#define rc_morph_erode_diamond_9x9_p_bin_IMPL                RC_IMPL_GEN
#define rc_morph_erode_diamond_9x9_p_bin_UNROLL              1
#define rc_morph_erode_diamond_9x9_p_bin_SCORE               0.0

#define rc_morph_dilate_diamond_9x9_p_bin_IMPL               RC_IMPL_GEN
#define rc_morph_dilate_diamond_9x9_p_bin_UNROLL             1
#define rc_morph_dilate_diamond_9x9_p_bin_SCORE              0.0

#define rc_morph_erode_diamond_13x13_p_bin_IMPL              RC_IMPL_GEN
#define rc_morph_erode_diamond_13x13_p_bin_UNROLL            1
#define rc_morph_erode_diamond_13x13_p_bin_SCORE             0.0

#define rc_morph_dilate_diamond_13x13_p_bin_IMPL             RC_IMPL_GEN
#define rc_morph_dilate_diamond_13x13_p_bin_UNROLL           1
#define rc_morph_dilate_diamond_13x13_p_bin_SCORE            0.0

#define rc_morph_erode_diamond_15x15_p_bin_IMPL              RC_IMPL_GEN
#define rc_morph_erode_diamond_15x15_p_bin_UNROLL            1
#define rc_morph_erode_diamond_15x15_p_bin_SCORE             0.0

#define rc_morph_dilate_diamond_15x15_p_bin_IMPL             RC_IMPL_GEN
#define rc_morph_dilate_diamond_15x15_p_bin_UNROLL           1
#define rc_morph_dilate_diamond_15x15_p_bin_SCORE            0.0

#define rc_morph_erode_diamond_17x17_p_bin_IMPL              RC_IMPL_GEN
#define rc_morph_erode_diamond_17x17_p_bin_UNROLL            1
#define rc_morph_erode_diamond_17x17_p_bin_SCORE             0.0

#define rc_morph_dilate_diamond_17x17_p_bin_IMPL             RC_IMPL_GEN
#define rc_morph_dilate_diamond_17x17_p_bin_UNROLL           1
#define rc_morph_dilate_diamond_17x17_p_bin_SCORE            0.0

#define rc_morph_erode_diamond_25x25_p_bin_IMPL              RC_IMPL_GEN
#define rc_morph_erode_diamond_25x25_p_bin_UNROLL            1
#define rc_morph_erode_diamond_25x25_p_bin_SCORE             0.0

#define rc_morph_dilate_diamond_25x25_p_bin_IMPL             RC_IMPL_GEN
#define rc_morph_dilate_diamond_25x25_p_bin_UNROLL           1
#define rc_morph_dilate_diamond_25x25_p_bin_SCORE            0.0

#define rc_morph_erode_diamond_29x29_p_bin_IMPL              RC_IMPL_GEN
#define rc_morph_erode_diamond_29x29_p_bin_UNROLL            1
#define rc_morph_erode_diamond_29x29_p_bin_SCORE             0.0

#define rc_morph_dilate_diamond_29x29_p_bin_IMPL             RC_IMPL_GEN
#define rc_morph_dilate_diamond_29x29_p_bin_UNROLL           1
#define rc_morph_dilate_diamond_29x29_p_bin_SCORE            0.0

#define rc_morph_erode_diamond_31x31_p_bin_IMPL              RC_IMPL_GEN
#define rc_morph_erode_diamond_31x31_p_bin_UNROLL            1
#define rc_morph_erode_diamond_31x31_p_bin_SCORE             0.0

#define rc_morph_dilate_diamond_31x31_p_bin_IMPL             RC_IMPL_GEN
#define rc_morph_dilate_diamond_31x31_p_bin_UNROLL           1
#define rc_morph_dilate_diamond_31x31_p_bin_SCORE            0.0

#define rc_morph_erode_octagon_5x5_p_bin_IMPL                RC_IMPL_GEN
#define rc_morph_erode_octagon_5x5_p_bin_UNROLL              1
#define rc_morph_erode_octagon_5x5_p_bin_SCORE               0.0

#define rc_morph_dilate_octagon_5x5_p_bin_IMPL               RC_IMPL_GEN
#define rc_morph_dilate_octagon_5x5_p_bin_UNROLL             1
#define rc_morph_dilate_octagon_5x5_p_bin_SCORE              0.0

#define rc_morph_erode_octagon_7x7_p_bin_IMPL                RC_IMPL_GEN
#define rc_morph_erode_octagon_7x7_p_bin_UNROLL              1
#define rc_morph_erode_octagon_7x7_p_bin_SCORE               0.0

#define rc_morph_dilate_octagon_7x7_p_bin_IMPL               RC_IMPL_GEN
#define rc_morph_dilate_octagon_7x7_p_bin_UNROLL             1
#define rc_morph_dilate_octagon_7x7_p_bin_SCORE              0.0

#define rc_morph_erode_octagon_9x9_p_bin_IMPL                RC_IMPL_GEN
#define rc_morph_erode_octagon_9x9_p_bin_UNROLL              1
#define rc_morph_erode_octagon_9x9_p_bin_SCORE               0.0

#define rc_morph_dilate_octagon_9x9_p_bin_IMPL               RC_IMPL_GEN
#define rc_morph_dilate_octagon_9x9_p_bin_UNROLL             1
#define rc_morph_dilate_octagon_9x9_p_bin_SCORE              0.0

#define rc_morph_erode_octagon_13x13_p_bin_IMPL              RC_IMPL_GEN
#define rc_morph_erode_octagon_13x13_p_bin_UNROLL            1
#define rc_morph_erode_octagon_13x13_p_bin_SCORE             0.0

#define rc_morph_dilate_octagon_13x13_p_bin_IMPL             RC_IMPL_GEN
#define rc_morph_dilate_octagon_13x13_p_bin_UNROLL           1
#define rc_morph_dilate_octagon_13x13_p_bin_SCORE            0.0

#define rc_morph_erode_octagon_15x15_p_bin_IMPL              RC_IMPL_GEN
#define rc_morph_erode_octagon_15x15_p_bin_UNROLL            1
#define rc_morph_erode_octagon_15x15_p_bin_SCORE             0.0

#define rc_morph_dilate_octagon_15x15_p_bin_IMPL             RC_IMPL_GEN
#define rc_morph_dilate_octagon_15x15_p_bin_UNROLL           1
#define rc_morph_dilate_octagon_15x15_p_bin_SCORE            0.0

#define rc_morph_erode_octagon_17x17_p_bin_IMPL              RC_IMPL_GEN
#define rc_morph_erode_octagon_17x17_p_bin_UNROLL            1
#define rc_morph_erode_octagon_17x17_p_bin_SCORE             0.0

#define rc_morph_dilate_octagon_17x17_p_bin_IMPL             RC_IMPL_GEN
#define rc_morph_dilate_octagon_17x17_p_bin_UNROLL           1
#define rc_morph_dilate_octagon_17x17_p_bin_SCORE            0.0

#define rc_morph_erode_octagon_25x25_p_bin_IMPL              RC_IMPL_GEN
#define rc_morph_erode_octagon_25x25_p_bin_UNROLL            1
#define rc_morph_erode_octagon_25x25_p_bin_SCORE             0.0

#define rc_morph_dilate_octagon_25x25_p_bin_IMPL             RC_IMPL_GEN
#define rc_morph_dilate_octagon_25x25_p_bin_UNROLL           1
#define rc_morph_dilate_octagon_25x25_p_bin_SCORE            0.0

#define rc_morph_erode_octagon_29x29_p_bin_IMPL              RC_IMPL_GEN
#define rc_morph_erode_octagon_29x29_p_bin_UNROLL            1
#define rc_morph_erode_octagon_29x29_p_bin_SCORE             0.0

#define rc_morph_dilate_octagon_29x29_p_bin_IMPL             RC_IMPL_GEN
#define rc_morph_dilate_octagon_29x29_p_bin_UNROLL           1
#define rc_morph_dilate_octagon_29x29_p_bin_SCORE            0.0

#define rc_morph_erode_octagon_31x31_p_bin_IMPL              RC_IMPL_GEN
#define rc_morph_erode_octagon_31x31_p_bin_UNROLL            1
#define rc_morph_erode_octagon_31x31_p_bin_SCORE             0.0

#define rc_morph_dilate_octagon_31x31_p_bin_IMPL             RC_IMPL_GEN
#define rc_morph_dilate_octagon_31x31_p_bin_UNROLL           1
#define rc_morph_dilate_octagon_31x31_p_bin_SCORE            0.0

#define rc_morph_erode_disc_7x7_bin_IMPL                     RC_IMPL_GEN
#define rc_morph_erode_disc_7x7_bin_UNROLL                   1
#define rc_morph_erode_disc_7x7_bin_SCORE                    0.0

#define rc_morph_dilate_disc_7x7_bin_IMPL                    RC_IMPL_GEN
#define rc_morph_dilate_disc_7x7_bin_UNROLL                  1
#define rc_morph_dilate_disc_7x7_bin_SCORE                   0.0

#define rc_morph_erode_disc_7x7_p_bin_IMPL                   RC_IMPL_GEN
#define rc_morph_erode_disc_7x7_p_bin_UNROLL                 1
#define rc_morph_erode_disc_7x7_p_bin_SCORE                  0.0

#define rc_morph_dilate_disc_7x7_p_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_dilate_disc_7x7_p_bin_UNROLL                1
#define rc_morph_dilate_disc_7x7_p_bin_SCORE                 0.0

#define rc_morph_erode_disc_9x9_p_bin_IMPL                   RC_IMPL_GEN
#define rc_morph_erode_disc_9x9_p_bin_UNROLL                 1
#define rc_morph_erode_disc_9x9_p_bin_SCORE                  0.0

#define rc_morph_dilate_disc_9x9_p_bin_IMPL                  RC_IMPL_GEN
#define rc_morph_dilate_disc_9x9_p_bin_UNROLL                1
#define rc_morph_dilate_disc_9x9_p_bin_SCORE                 0.0

#define rc_morph_erode_disc_11x11_p_bin_IMPL                 RC_IMPL_GEN
#define rc_morph_erode_disc_11x11_p_bin_UNROLL               1
#define rc_morph_erode_disc_11x11_p_bin_SCORE                0.0

#define rc_morph_dilate_disc_11x11_p_bin_IMPL                RC_IMPL_GEN
#define rc_morph_dilate_disc_11x11_p_bin_UNROLL              1
#define rc_morph_dilate_disc_11x11_p_bin_SCORE               0.0

#define rc_morph_erode_disc_13x13_p_bin_IMPL                 RC_IMPL_GEN
#define rc_morph_erode_disc_13x13_p_bin_UNROLL               1
#define rc_morph_erode_disc_13x13_p_bin_SCORE                0.0

#define rc_morph_dilate_disc_13x13_p_bin_IMPL                RC_IMPL_GEN
#define rc_morph_dilate_disc_13x13_p_bin_UNROLL              1
#define rc_morph_dilate_disc_13x13_p_bin_SCORE               0.0

#define rc_morph_erode_disc_15x15_p_bin_IMPL                 RC_IMPL_GEN
#define rc_morph_erode_disc_15x15_p_bin_UNROLL               1
#define rc_morph_erode_disc_15x15_p_bin_SCORE                0.0

#define rc_morph_dilate_disc_15x15_p_bin_IMPL                RC_IMPL_GEN
#define rc_morph_dilate_disc_15x15_p_bin_UNROLL              1
#define rc_morph_dilate_disc_15x15_p_bin_SCORE               0.0

#define rc_morph_erode_disc_17x17_p_bin_IMPL                 RC_IMPL_GEN
#define rc_morph_erode_disc_17x17_p_bin_UNROLL               1
#define rc_morph_erode_disc_17x17_p_bin_SCORE                0.0

#define rc_morph_dilate_disc_17x17_p_bin_IMPL                RC_IMPL_GEN
#define rc_morph_dilate_disc_17x17_p_bin_UNROLL              1
#define rc_morph_dilate_disc_17x17_p_bin_SCORE               0.0

#define rc_morph_erode_disc_19x19_p_bin_IMPL                 RC_IMPL_GEN
#define rc_morph_erode_disc_19x19_p_bin_UNROLL               1
#define rc_morph_erode_disc_19x19_p_bin_SCORE                0.0

#define rc_morph_dilate_disc_19x19_p_bin_IMPL                RC_IMPL_GEN
#define rc_morph_dilate_disc_19x19_p_bin_UNROLL              1
#define rc_morph_dilate_disc_19x19_p_bin_SCORE               0.0

#define rc_morph_erode_disc_25x25_p_bin_IMPL                 RC_IMPL_GEN
#define rc_morph_erode_disc_25x25_p_bin_UNROLL               1
#define rc_morph_erode_disc_25x25_p_bin_SCORE                0.0

#define rc_morph_dilate_disc_25x25_p_bin_IMPL                RC_IMPL_GEN
#define rc_morph_dilate_disc_25x25_p_bin_UNROLL              1
#define rc_morph_dilate_disc_25x25_p_bin_SCORE               0.0

#define rc_morph_hmt_golay_l_3x3_c48_r0_bin_IMPL             RC_IMPL_GEN
#define rc_morph_hmt_golay_l_3x3_c48_r0_bin_UNROLL           1
#define rc_morph_hmt_golay_l_3x3_c48_r0_bin_SCORE            0.0

#define rc_morph_hmt_golay_l_3x3_c48_r90_bin_IMPL            RC_IMPL_GEN
#define rc_morph_hmt_golay_l_3x3_c48_r90_bin_UNROLL          1
#define rc_morph_hmt_golay_l_3x3_c48_r90_bin_SCORE           0.0

#define rc_morph_hmt_golay_l_3x3_c48_r180_bin_IMPL           RC_IMPL_GEN
#define rc_morph_hmt_golay_l_3x3_c48_r180_bin_UNROLL         1
#define rc_morph_hmt_golay_l_3x3_c48_r180_bin_SCORE          0.0

#define rc_morph_hmt_golay_l_3x3_c48_r270_bin_IMPL           RC_IMPL_GEN
#define rc_morph_hmt_golay_l_3x3_c48_r270_bin_UNROLL         1
#define rc_morph_hmt_golay_l_3x3_c48_r270_bin_SCORE          0.0

#define rc_morph_hmt_golay_l_3x3_c4_r45_bin_IMPL             RC_IMPL_GEN
#define rc_morph_hmt_golay_l_3x3_c4_r45_bin_UNROLL           1
#define rc_morph_hmt_golay_l_3x3_c4_r45_bin_SCORE            0.0

#define rc_morph_hmt_golay_l_3x3_c4_r135_bin_IMPL            RC_IMPL_GEN
#define rc_morph_hmt_golay_l_3x3_c4_r135_bin_UNROLL          1
#define rc_morph_hmt_golay_l_3x3_c4_r135_bin_SCORE           0.0

#define rc_morph_hmt_golay_l_3x3_c4_r225_bin_IMPL            RC_IMPL_GEN
#define rc_morph_hmt_golay_l_3x3_c4_r225_bin_UNROLL          1
#define rc_morph_hmt_golay_l_3x3_c4_r225_bin_SCORE           0.0

#define rc_morph_hmt_golay_l_3x3_c4_r315_bin_IMPL            RC_IMPL_GEN
#define rc_morph_hmt_golay_l_3x3_c4_r315_bin_UNROLL          1
#define rc_morph_hmt_golay_l_3x3_c4_r315_bin_SCORE           0.0

#define rc_morph_hmt_golay_l_3x3_c8_r45_bin_IMPL             RC_IMPL_GEN
#define rc_morph_hmt_golay_l_3x3_c8_r45_bin_UNROLL           1
#define rc_morph_hmt_golay_l_3x3_c8_r45_bin_SCORE            0.0

#define rc_morph_hmt_golay_l_3x3_c8_r135_bin_IMPL            RC_IMPL_GEN
#define rc_morph_hmt_golay_l_3x3_c8_r135_bin_UNROLL          1
#define rc_morph_hmt_golay_l_3x3_c8_r135_bin_SCORE           0.0

#define rc_morph_hmt_golay_l_3x3_c8_r225_bin_IMPL            RC_IMPL_GEN
#define rc_morph_hmt_golay_l_3x3_c8_r225_bin_UNROLL          1
#define rc_morph_hmt_golay_l_3x3_c8_r225_bin_SCORE           0.0

#define rc_morph_hmt_golay_l_3x3_c8_r315_bin_IMPL            RC_IMPL_GEN
#define rc_morph_hmt_golay_l_3x3_c8_r315_bin_UNROLL          1
#define rc_morph_hmt_golay_l_3x3_c8_r315_bin_SCORE           0.0

#define rc_morph_hmt_golay_e_3x3_c4_r0_bin_IMPL              RC_IMPL_GEN
#define rc_morph_hmt_golay_e_3x3_c4_r0_bin_UNROLL            1
#define rc_morph_hmt_golay_e_3x3_c4_r0_bin_SCORE             0.0

#define rc_morph_hmt_golay_e_3x3_c4_r90_bin_IMPL             RC_IMPL_GEN
#define rc_morph_hmt_golay_e_3x3_c4_r90_bin_UNROLL           1
#define rc_morph_hmt_golay_e_3x3_c4_r90_bin_SCORE            0.0

#define rc_morph_hmt_golay_e_3x3_c4_r180_bin_IMPL            RC_IMPL_GEN
#define rc_morph_hmt_golay_e_3x3_c4_r180_bin_UNROLL          1
#define rc_morph_hmt_golay_e_3x3_c4_r180_bin_SCORE           0.0

#define rc_morph_hmt_golay_e_3x3_c4_r270_bin_IMPL            RC_IMPL_GEN
#define rc_morph_hmt_golay_e_3x3_c4_r270_bin_UNROLL          1
#define rc_morph_hmt_golay_e_3x3_c4_r270_bin_SCORE           0.0

#define rc_morph_hmt_golay_e_3x3_c8_r0_bin_IMPL              RC_IMPL_GEN
#define rc_morph_hmt_golay_e_3x3_c8_r0_bin_UNROLL            1
#define rc_morph_hmt_golay_e_3x3_c8_r0_bin_SCORE             0.0

#define rc_morph_hmt_golay_e_3x3_c8_r90_bin_IMPL             RC_IMPL_GEN
#define rc_morph_hmt_golay_e_3x3_c8_r90_bin_UNROLL           1
#define rc_morph_hmt_golay_e_3x3_c8_r90_bin_SCORE            0.0

#define rc_morph_hmt_golay_e_3x3_c8_r180_bin_IMPL            RC_IMPL_GEN
#define rc_morph_hmt_golay_e_3x3_c8_r180_bin_UNROLL          1
#define rc_morph_hmt_golay_e_3x3_c8_r180_bin_SCORE           0.0

#define rc_morph_hmt_golay_e_3x3_c8_r270_bin_IMPL            RC_IMPL_GEN
#define rc_morph_hmt_golay_e_3x3_c8_r270_bin_UNROLL          1
#define rc_morph_hmt_golay_e_3x3_c8_r270_bin_SCORE           0.0

#define rc_morph_hmt_golay_e_3x3_c8_r45_bin_IMPL             RC_IMPL_GEN
#define rc_morph_hmt_golay_e_3x3_c8_r45_bin_UNROLL           1
#define rc_morph_hmt_golay_e_3x3_c8_r45_bin_SCORE            0.0

#define rc_morph_hmt_golay_e_3x3_c8_r135_bin_IMPL            RC_IMPL_GEN
#define rc_morph_hmt_golay_e_3x3_c8_r135_bin_UNROLL          1
#define rc_morph_hmt_golay_e_3x3_c8_r135_bin_SCORE           0.0

#define rc_morph_hmt_golay_e_3x3_c8_r225_bin_IMPL            RC_IMPL_GEN
#define rc_morph_hmt_golay_e_3x3_c8_r225_bin_UNROLL          1
#define rc_morph_hmt_golay_e_3x3_c8_r225_bin_SCORE           0.0

#define rc_morph_hmt_golay_e_3x3_c8_r315_bin_IMPL            RC_IMPL_GEN
#define rc_morph_hmt_golay_e_3x3_c8_r315_bin_UNROLL          1
#define rc_morph_hmt_golay_e_3x3_c8_r315_bin_SCORE           0.0

#define rc_margin_horz_bin_IMPL                              RC_IMPL_SIMD
#define rc_margin_horz_bin_UNROLL                            1
#define rc_margin_horz_bin_SCORE                             0.0

#define rc_margin_vert_bin_IMPL                              RC_IMPL_GEN
#define rc_margin_vert_bin_UNROLL                            1
#define rc_margin_vert_bin_SCORE                             0.0

#define rc_cond_set_u8_IMPL                                  RC_IMPL_SIMD
#define rc_cond_set_u8_UNROLL                                1
#define rc_cond_set_u8_SCORE                                 0.0

#define rc_cond_addc_u8_IMPL                                 RC_IMPL_SIMD
#define rc_cond_addc_u8_UNROLL                               1
#define rc_cond_addc_u8_SCORE                                0.0

#define rc_cond_subc_u8_IMPL                                 RC_IMPL_SIMD
#define rc_cond_subc_u8_UNROLL                               1
#define rc_cond_subc_u8_SCORE                                0.0

#define rc_cond_copy_u8_IMPL                                 RC_IMPL_SIMD
#define rc_cond_copy_u8_UNROLL                               1
#define rc_cond_copy_u8_SCORE                                0.0

#define rc_cond_add_u8_IMPL                                  RC_IMPL_SIMD
#define rc_cond_add_u8_UNROLL                                1
#define rc_cond_add_u8_SCORE                                 0.0

#endif /* RAPPTUNE_H */
